LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity vhdlOnesComplementer is
	port(comp, x0, x1, x2, x3, x4, x5, x6, x7: in STD_LOGIC;
			s0, s1, s2, s3, s4, s5, s6, s7: out STD_LOGIC);
end vhdlOnesComplementer;
--1's complement is just s_n=exclusive or of x_n and comp
architecture complementerLogic of vhdlOnesComplementer is
begin
	s0<= x0 xor comp;
	s1<= x2 xor comp;
	s2<= x2 xor comp;
	s3<= x3 xor comp;
	s4<= x4 xor comp;
	s5<= x5 xor comp;
	s6<= x6 xor comp;
	s7<= x7 xor comp;

end complementerLogic;