LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity vhdlFullAdder is
	port (cin, x, y : IN STD_LOGIC;
				s, cout : out STD_LOGIC);
END vhdlFullAdder;


architecture logicFunc of vhdlFullAdder is
begin
	s <= x xor y xor cin;
	cout <= (x and y) or (cin and x) or (cin and y);
end logicFunc;	