onerror {exit -code 1}
vlib work
vlog -work work lab4.vo
vlog -work work fullAdderWaveform.vwf.vt
vsim -novopt -c -t 1ps -L cycloneiii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.fullAdder_vlg_vec_tst -voptargs="+acc"
vcd file -direction lab4.msim.vcd
vcd add -internal fullAdder_vlg_vec_tst/*
vcd add -internal fullAdder_vlg_vec_tst/i1/*
run -all
quit -f
