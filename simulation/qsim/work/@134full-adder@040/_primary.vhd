library verilog;
use verilog.vl_types.all;
entity \full-adder\ is
    port(
        cout            : out    vl_logic;
        x               : in     vl_logic;
        y               : in     vl_logic;
        cin             : in     vl_logic;
        s               : out    vl_logic
    );
end \full-adder\;
